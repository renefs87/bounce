import pyglet
from math import sin, cos, pi, sqrt
twopi = 2*pi

from Display import Display

class PygletDisplay(Display):

    def __init__(self, p):
        super().__init__(p)
        self.window = pyglet.window.Window(600,400)
        self.fps_display = pyglet.clock.ClockDisplay()
        self.window.event(self.on_draw)

    def go(self):
        pyglet.clock.schedule_interval(self.update, 1/60.0)
        pyglet.app.run()

    def on_draw(self):
        self.window.clear()
        for p in self.ps:
            self.draw_circle(p.x, p.y, p.r, p.c)
        self.fps_display.draw()

    @property
    def bounding_box(self):
        return 0, self.window.width, 0, self.window.height

    def draw_circle(self, x,y, r, c):
        segments = 20
        def circle_vertices():
            delta_angle = twopi / segments
            angle = 0
            while angle < twopi:
                yield x + r * cos(angle)
                yield y + r * sin(angle)
                angle += delta_angle

        pyglet.gl.glColor3f(*c.as_rgb_01())
        pyglet.graphics.draw(segments, pyglet.gl.GL_LINE_LOOP,
                             ('v2f', tuple(circle_vertices())))
