import tkinter as tk

from Display import Display

class TkinterDisplay(Display):

    def __init__(self, p):
        super().__init__(p)
        master = tk.Tk()
        self.w = tk.Canvas(master, width=600, height=400, bg='black')
        self.w.pack()


    def go(self):
        self.update(0)
        tk.mainloop()

    @property
    def bounding_box(self):
        return 0, self.w.winfo_width(), 0, self.w.winfo_height()

    def update(self, dt):
        super().update(dt)
        self.w.delete(tk.ALL)
        for p in self.ps:
            self.draw_circle(p.x, p.y, p.r, p.c)
        self.w.update()
        self.w.after(17, self.update, 1/60.0)

    def draw_circle(self, x,y, r, c):
        self.w.create_oval(x-r, y-r,  x+r, y+r, outline='#'+c.as_rgb_f())
