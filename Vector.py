from math import sqrt

from operator import add, sub, eq, ne, neg

def Vector(spec):

    class Vector(object):

        def __init__(self, *args):
            if len(args) != len(spec):
                raise TypeError
            self._data = list(args)

        def __getitem__(self, n): return self._data[n]
        def __setitem__(self, n, value): self._data[n] = value

        def __add__(self, other):  return Vector(*map(add, self, other))
        def __sub__(self, other):  return Vector(*map(sub, self, other))
        def __eq__ (self, other):  return    all( map(eq,  self, other))
        def __ne__ (self, other):  return    any( map(ne,  self, other))
        def __neg__(self):         return Vector(*map(neg, self))
        def __mul__(self, scalar): return Vector(*(x * scalar for x in self))
        def __div__(self, scalar): return Vector(*(x / scalar for x in self))
        def __abs__(self):         return        sqrt(sum(x*x for x in self))
        __rmul__    = __mul__
        __truediv__ = __div__

        def __repr__(self):
            CLASS = Vector.__name__
            args  = ','.join(map(str, self))
            return f'{CLASS}({args})'


    name = "Vector('{}')".format(spec)
    Vector.__name__ = name

    for n, name in enumerate(spec):
        setattr(Vector, name, property(*make_accessors(n, name)))

    return Vector

def make_accessors(n, name):
    def get(self): return self[n]
    def set(self, value): self[n] = value
    return get, set

Vector2D = Vector('xy')
Vector3D = Vector('xyz')

Vector2D.__name__ = 'Vector2D'
Vector3D.__name__ = 'Vector3D'
