def assert_evolution_gen(get_current_value, compare):
    def step():
        old = get_current_value()
        yield
        while True:
            new = get_current_value()
            assert compare(new, old)
            yield
            old = new
    return step().__next__


class assert_evolution_class:

    def __init__(self, get_current_value, compare):
        self._get_current_value = get_current_value
        self._compare           = compare

    def __call__(self):
        new = self._get_current_value()
        if not hasattr(self, '_old'):
            self._old = new
            return
        assert self._compare(new, self._old)
        self._old = new


def assert_evolution_closure(get_current_value, compare):
    first_call = True
    old = None
    def step():
        nonlocal first_call, old
        new = get_current_value()
        if first_call:
            first_call = False
        else:
            assert compare(new, old)
        old = new
    return step

assert_evolution = assert_evolution_gen
